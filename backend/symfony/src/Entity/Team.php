<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class Entity
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class Team
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $logo;

    /**
     * @var User[]
     *
     * Many Teams have Many Players (Users).
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="users_teams",
     *      joinColumns={@ORM\JoinColumn(name="team_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $players = [];

    public function __construct($name, $logo)
    {
        $this->name = $name;
        $this->logo = $logo;
    }


    public static function create($name, $logo)
    {
        return new self($name, $logo);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $players
     */
    public function setPlayers($players): void
    {
        $this->players = $players;
    }

    /**
     * @return mixed
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo): void
    {
        $this->logo = $logo;
    }
}