<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 *
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */    /**
 * @ORM\Column(type="string")
 */
    private $password;


    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isPlayer = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $imageSmall = '';

    /**
     * @param $email
     * @param $password
     * @return User
     */
    public static function create($email, $password, $firstName, $lastName)
    {
        $person = new self($email);
        $person->setEmail($email);
        $person->setPassword($password);

        $person->firstName = $firstName;
        $person->lastName = $lastName;

        return $person;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getRoleForFrontend()
    {
        if (in_array('ROLE_ADMIN', $this->roles )) {
            return 'Admin';
        }

        return 'User';
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return bool
     */
    public function isPlayer(): bool
    {
        return $this->isPlayer;
    }

    /**
     * @param bool $isPlayer
     */
    public function setIsPlayer(bool $isPlayer): void
    {
        $this->isPlayer = $isPlayer;
    }

    public function checkIsPlayer($user): bool
    {
        return $user->isPlayer() === true;
    }

    /**
     * @return string
     */
    public function getImageSmall(): string
    {
        return $this->imageSmall;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @param string $imageSmall
     */
    public function setImageSmall(string $imageSmall): void
    {
        $this->imageSmall = $imageSmall;
    }
}
