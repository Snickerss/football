<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Security\Guard\AuthenticatorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/api/register", methods={"POST"})
     */
    public function register(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        $firstName = $request->request->get('firstName');
        $lastName = $request->request->get('lastName');


        $user = User::create($email, $password, $firstName, $lastName);
        $password = $encoder->encodePassword($user, $password);
        $user->setPassword($password);

        $userRepository->add($user);
        $userRepository->save();

        return new JsonResponse();
    }

    /**
     * @Route("/api/me", methods={"GET"})
     * @return JsonResponse
     */
    public function me()
    {
        return new JsonResponse([
            'email' => $this->getUser()->getEmail()
        ]);
    }
}