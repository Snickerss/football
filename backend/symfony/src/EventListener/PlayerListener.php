<?php


namespace App\EventListener;


use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Trivago\Jade\Application\JsonApi\Request\CollectionRequest;
use Trivago\Jade\Application\JsonApi\Request\CreateRequest;
use Trivago\Jade\Application\JsonApi\Request\DeleteRequest;
use Trivago\Jade\Application\JsonApi\Request\EntityRequest;
use Trivago\Jade\Application\JsonApi\Request\UpdateRequest;
use Trivago\Jade\Application\Listener\RequestListener;
use Trivago\Jade\Domain\Resource\ExpressionFilter;
use Trivago\Jade\Domain\Resource\Path;

class PlayerListener implements RequestListener
{
    private $authUser;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->authUser  = $tokenStorage->getToken()->getUser();
    }

    public function onGetEntityRequest(EntityRequest $request)
    {
        // TODO: Implement onGetEntityRequest() method.
    }

    /**
     * Для Players выполняется фильтрация по полю isPlayer=true
     *
     * @param CollectionRequest $request
     * @return CollectionRequest
     */
    public function onGetCollectionRequest(CollectionRequest $request)
    {
        if (!$this->authUser instanceof User) {
            $constraint = $request->getConstraint();

            $path = new Path('isPlayer');
            $filterForPlayers = new ExpressionFilter($path, 'eq', true);
            $constraint->getFilterCollection()->addFilter($filterForPlayers);
        }

        return $request;
    }

    public function onCreateRequest(CreateRequest $request)
    {
        // TODO: Implement onCreateRequest() method.
    }

    public function onUpdateRequest(UpdateRequest $request)
    {
        // TODO: Implement onUpdateRequest() method.
    }

    public function onDeleteRequest(DeleteRequest $request)
    {
        // TODO: Implement onDeleteRequest() method.
    }

    public function supports($resourceName)
    {
        return $resourceName === 'players';
    }

}