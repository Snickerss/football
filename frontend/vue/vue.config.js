const path = require('path');

module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  chainWebpack: config => {
    //this path is specific to my project
    config.resolve.alias.set('@', path.resolve('src'));
  },
}
