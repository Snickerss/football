import Vue from 'vue'
import Router from 'vue-router'
import Tournaments from '@/components/landing/tournament/Tournaments'
import Games from '@/components/landing/game/Games'
import Players from '@/components/landing/player/Players'
import Login from '@/components/landing/login/Login'

import UserList from '@/components/acp/user/List'
import TournamentList from '@/components/acp/tournament/List'
import TeamList from '@/components/acp/team/List'


import { authentication } from '@/store/authentication'

import { Role } from '../helpers/roles';

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Tournaments
    },
    {
      path: '/tournaments',
      name: 'tournaments',
      component: Tournaments
    },
    {
      path: '/games',
      name: 'games',
      component: Games
    },
    {
      path: '/players',
      name: 'games',
      component: Players
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },


    /////////////////////////////////////////////////////
    {
      path: '/acp',
      name: 'acp',
      component: UserList,
      meta: { authorize: [Role.Admin] }
    },
    {
      path: '/acp/users',
      name: 'acpUsers',
      component: UserList,
      meta: { authorize: [Role.Admin] }
    },
    {
      path: '/acp/tournaments',
      name: 'acpTournaments',
      component: TournamentList,
      meta: { authorize: [Role.Admin] }
    },
    {
      path: '/acp/teams',
      name: 'acpTeams',
      component: TeamList,
      meta: { authorize: [Role.Admin] }
    },
  ]
});

router.beforeEach((to, from, next) => {
  // // redirect to login page if not logged in and trying to access a restricted page
  const { authorize } = to.meta;
  const currentUser = authentication.state.user;

  if (authorize) {
    if (!currentUser) {
      // not logged in so redirect to login page with the return url
      return next({ path: '/login' });
    }

    // check if route is restricted by role
    if (authorize.length) {
      if (!authorize.includes(currentUser.role)) {
        // role not authorised so redirect to home page
        return next({ path: '/login' });
      }
    }
  }

  if (currentUser) {
    if (to.name == 'login') return next({ path: '/' });
  }

  next();
})

export default router
