import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import "vuetify/dist/vuetify.css";
import router from './router'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import { store } from './store';
import indexApi from './api/index';
import Axios from 'axios'

import VueRouter from 'vue-router';
Vue.use(VueRouter);

Vue.config.productionTip = false;

Vue.prototype.$http = Axios;
indexApi.setHttp(Vue.prototype.$http);

// register globally
import infiniteScroll from 'vue-infinite-scroll'
Vue.use(infiniteScroll)

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App),
  directives: {infiniteScroll}
}).$mount('#app')
