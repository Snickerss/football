import api from './index'

export default {
  getAllChampionships(params) {
    return api.get('/api/championships', params)
  },
  // addTournament(tournament) {
  //   return api.post('/api/tournaments', tournament, true)
  // },
  // updateTournament(tournament) {
  //   return api.patch('/api/tournaments/'+tournament.data.id, tournament, true)
  // },
  // getTournament(id) {
  //   return api.get('/api/tournaments/'+id)
  // },
  // removeTournament(id) {
  //   return api.delete('/api/tournaments/'+id)
  // },
}
