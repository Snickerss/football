import api from './index'

export default {
  getAllTournamentsForAcp(sort, params, needAuth) {
    return api.get('/api/tournaments'+sort, params, needAuth)
  },
  createTournament(attributes) {
    let tournament = {
      "data": {
        "type": "tournaments",
        "attributes": attributes
      }
    };

    return api.post('/api/tournaments', tournament, true)
  },
  updateTournament(attributes) {
    let tournament = {
      "data": attributes
    };

    return api.patch('/api/tournaments/'+attributes.id, tournament, true)
  },
  getTournament(id) {
    return api.get('/api/tournaments/'+id)
  },
  removeTournament(id) {
    return api.delete('/api/tournaments/'+id)
  },
}
