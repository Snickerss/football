import axios from 'axios';
import {authentication} from '../store/authentication';

let baseHttp;

export default {
  post(url, params, needAuth) {
    return this.base(needAuth).post(url, params);
  },
  patch(url, params, needAuth) {
    return this.base(needAuth).patch(url, params);
  },
  get(url, params, needAuth) {
    return this.base(needAuth).get(url, params);
  },
  delete(url, needAuth) {
    return this.base(needAuth).delete(url)
  },
  base(needAuth) {
    const BASE_URL = 'http://localhost';

    const jwtToken = authentication.getters.getJWTToken();

    let headers = {
      'X-Requested-With': 'XMLHttpRequest',
    };

    if (jwtToken !== null) {
      if (needAuth) {
        baseHttp.defaults.headers.common['Authorization'] = 'Bearer ' + jwtToken
      } else {
        baseHttp.defaults.headers.common['Authorization'] = ''
      }
    }

    return baseHttp;
  },

  setHttp(http) {
    const BASE_URL = 'http://localhost';

    const jwtToken = authentication.getters.getJWTToken();

    let headers = {
      'X-Requested-With': 'XMLHttpRequest',
    };

    if (jwtToken !== null) {
      headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Bearer ' + jwtToken
      };
    }

    baseHttp = http.create({
      baseURL: BASE_URL,
      // headers: headers
    });
  },
  getHttp() {
    return baseHttp;
  }
}
