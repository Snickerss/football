import api from './index'

export default {
  me() {
    return api.get('/api/me')
  },
  checkLogin(data) {
    return api.post('/api/login_check', data)
  },
  register(data) {
    return api.post('/api/register', data)
  },
  getAllUsers(sort) {
    return api.get('/api/users'+sort, {}, true)
  },
  getAllPlayers(sort, params, needAuth) {
    return api.get('/api/players'+sort, params, needAuth)
  },

  createUser(attributes) {
    let user = {
      "data": {
        "type": "players",
        "attributes": attributes
      }
    };

    return api.post('/api/players', user, true)
  },

  updateUser(attributes) {
    let user = {
      "data": attributes
    };

    return api.patch('/api/players/'+attributes.id, user, true)
  }
}
