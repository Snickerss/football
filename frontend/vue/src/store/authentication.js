import apiUser from '../api/user'

const user = JSON.parse(localStorage.getItem('user'));

const initialState = user
  ? { status: { loggedIn: true }, user }
  : { status: {}, user: null };


export const authentication = {
  namespaced: true,
  state: initialState,
  actions: {
    async login({ dispatch, commit, rootState }, { email, password }) {

      const data = {
        "email" : email,
        "password" : password
      };

      try {
        let res = await apiUser.checkLogin(data);
        let user = res.data.data.user;

        localStorage.setItem('user', JSON.stringify(user));

        commit('loginSuccess', user);
        commit('changeToken', user.token, { root: true });
      } catch (e) {
        commit('loginFailure', error);
        throw new Error();
      }
    },
    logout({ commit, rootState }) {
      commit('changeToken', '', { root: true });
      commit('logout');
    },
  },
  mutations: {
    loginSuccess(state, user) {
      state.status = { loggedIn: true };
      state.user = user;
    },
    loginFailure(state) {
      state.status = {};
      state.user = null;
    },
    logout(state) {
      state.status = {};
      state.user = null;

      localStorage.removeItem('user');
    }
  },
  getters: {
    isUserloggedIn: state => {
      return state.status.loggedIn === true;
    },
    isUserAdmin: state => {
      if (state.user === null) return null;
      return state.user.role === 'Admin';
    },
    getJWTToken: state => {
      if (user === null) return null;

      return user.token;
    }
  }
}
