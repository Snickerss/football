import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import {authentication} from './authentication'
import {jsonapiModule} from 'jsonapi-vuex'

Vue.use(Vuex);
const jwtToken = authentication.getters.getJWTToken();

let api = axios.create({
  baseURL: 'http://localhost/api',
  headers: {
    'Content-Type': 'application/vnd.api+json'
  },
})

let apiAuth = axios.create({
  baseURL: 'http://localhost/api',
  headers: {
    'Content-Type': 'application/vnd.api+json',
    'Authorization': 'Bearer ' + jwtToken
  },
})

export const store = new Vuex.Store({
  state: {
    api: api,
    apiAuth: apiAuth,
  },
  modules: {
    authentication,
    jv: jsonapiModule(api),
    jvAuth: jsonapiModule(apiAuth)
  },
  mutations: {
    changeToken(state, token ) {
      // изменяем состояние
      state.apiAuth.defaults.headers['Authorization'] = 'Bearer ' + token
      console.log(state.apiAuth.defaults.headers)
    }
  }
})
